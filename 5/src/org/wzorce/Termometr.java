package org.wzorce;

import java.util.ArrayList;
import java.util.Observable;

/**
 * Created by natan on 14.04.16.
 */
public class Termometr extends Observable implements VisitableElement {
    // TODO: 15.04.16 refactor to double 
    private double currentTemperature = 0;
    private ArrayList<TemperatureObserver> listaObserwatorow;

    public Termometr(double stopnie) {
        currentTemperature = stopnie;
        listaObserwatorow = new ArrayList<>();
    }

    public double getCurrentTemperature() {
        return currentTemperature;
    }

    @Override
    public void notifyObservers(Object o) {
        for (TemperatureObserver obserwator : listaObserwatorow) {
            obserwator.update(this, o);
        }
    }

    public void setCurrentTemperature(double currentTemperature) {
        this.currentTemperature = currentTemperature;
        notifyObservers(currentTemperature);
    }

    public void addTemperatureObserver (TemperatureObserver o) {
        listaObserwatorow.add(o);
    }

    @Override
    public void acceptVisitor(Visitor v){
        v.performOperation(this);
    }
}
