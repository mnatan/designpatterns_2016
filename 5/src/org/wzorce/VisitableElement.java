package org.wzorce;

/**
 * Created by natan on 14.04.16.
 */
public interface VisitableElement {
    void acceptVisitor(Visitor v);
}
