package org.wzorce.test;

import org.junit.Before;
import org.junit.Test;
import org.wzorce.DetectorNet;
import org.wzorce.Exceptions.RoomNotFoundException;

import java.util.Observable;

import static org.junit.Assert.*;

/**
 * Created by natan on 15.04.16.
 */
public class DetectorNetTest {
    DetectorNet dom = new DetectorNet();

    @Before
    public void setUp() throws Exception {
        dom = new DetectorNet();
        dom.createRoom("kuchnia");
    }

    @Test
    public void testRoom() throws Exception {
        assertTrue(dom.room("kuchnia") != null);
        assertTrue(dom.room("łazienka") == null);
    }

    @Test
    public void testAddDetector() throws Exception {
        try {
            dom.addDetector("kuchnia", new Observable());
        } catch (RoomNotFoundException e) {
            e.printStackTrace();
        }
        assertTrue(dom.room("kuchnia").get(0) instanceof Observable);
    }

    @Test(expected = RoomNotFoundException.class)
    public void testAddDetectorEx() throws RoomNotFoundException {
        dom.addDetector("łazienka", new Observable());
    }
}