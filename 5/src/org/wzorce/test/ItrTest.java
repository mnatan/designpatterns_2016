package org.wzorce.test;

import org.wzorce.DetectorNet;
import org.wzorce.Exceptions.RoomNotFoundException;
import org.wzorce.SmokeDetector;
import org.wzorce.Termometr;

import java.util.Iterator;
import java.util.Observable;
import java.util.stream.IntStream;

import static org.junit.Assert.*;

/**
 * Created by natan on 14.04.16.
 */
public class ItrTest {

    DetectorNet detectors = new DetectorNet();

    @org.junit.Before
    public void setUp() throws Exception {
        detectors = new DetectorNet();
        detectors.createRoom("kuchnia");
        detectors.createRoom("łazienka");
        IntStream.range(0,3).forEach(n -> {
            try {
                detectors.addDetector("łazienka", new SmokeDetector());
                detectors.addDetector("kuchnia", new Termometr(n));
            } catch (RoomNotFoundException e) {
                e.printStackTrace();
            }
        });
    }

    @org.junit.Test
    public void testHasNext() throws Exception {
        Iterator i = detectors.iterator();
        assertTrue(i.hasNext());
        IntStream.range(0,6).forEach(n -> {
            assertTrue(i.hasNext());
            i.next();
        });
        assertFalse(i.hasNext());
    }

    @org.junit.Test
    public void testNext() throws Exception {
        Iterator i = detectors.iterator();
        while (i.hasNext()) {
            Observable o = (Observable) i.next();
            assertTrue(o != null);
        }
        assertTrue(i.next() == null);
    }

    @org.junit.Test
    public void emptySet() {
        detectors = new DetectorNet();
        Iterator i = detectors.iterator();
        assertFalse(i.hasNext());
        assertTrue(i.next() == null);
    }

}