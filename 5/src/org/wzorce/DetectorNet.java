package org.wzorce;

import org.wzorce.Exceptions.RoomNotFoundException;

import java.util.*;

/**
 * Created by natan on 14.04.16.
 */
public class DetectorNet {
    private HashMap<String, ArrayList<Observable>> rooms = new HashMap<>();

    public void createRoom(String name) {
        rooms.put(name, new ArrayList<>());
    }

    public ArrayList<Observable> room(String number) {
        return rooms.get(number);
    }

    public Itr iterator () {
        return new Itr(rooms.keySet());
    }

    public void addDetector(String room, Observable detector) throws RoomNotFoundException {
        try {
            rooms.get(room).add(detector);
        } catch (NullPointerException e){
            throw new RoomNotFoundException();
        }
    }

    public void printStructure(){
        Iterator it = rooms.entrySet().iterator();
        System.out.println("----- ROOMS -----------");
        while (it.hasNext()){
            Map.Entry pair = (Map.Entry) it.next();
            System.out.println(pair.getKey() + ": ");
            for (Observable o : (ArrayList<Observable>)pair.getValue()){
                System.out.println("  - " + o.getClass().toString());
            }
        }
        System.out.println("----- /ROOMS ----------");
    }

    private class Itr implements Iterator {
        ArrayList<String> keys = new ArrayList<>();
        Iterator subiter;

        public Itr(Set<String> strings) {
            keys.addAll(strings);
            if(!strings.isEmpty()){
                subiter = rooms.get(keys.get(0)).iterator();
                keys.remove(0);
            } else {
                subiter = new Iterator() {
                    @Override
                    public boolean hasNext() {
                        return false;
                    }
                    @Override
                    public Object next() {
                        return null;
                    }
                };
            }
        }

        @Override
        public boolean hasNext() {
            if (!subiter.hasNext() && keys.isEmpty()) return false;
            return true;
        }

        @Override
        public Observable next() {
            if (subiter.hasNext()) {
               return (Observable) subiter.next();
            } else if (!keys.isEmpty()){
                subiter = rooms.get(keys.get(0)).iterator();
                keys.remove(0);
                return (Observable) subiter.next();
            } else {
                return null;
            }
        }
    }
}
