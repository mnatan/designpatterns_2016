package org.wzorce;

import java.util.ArrayList;
import java.util.Observable;

/**
 * Created by natan on 14.04.16.
 */
public class SmokeDetector extends Observable implements VisitableElement {
    private boolean smokeDetected;
    private ArrayList<SmokeObserver> listaObserwatorow;

    public SmokeDetector() {
        listaObserwatorow = new ArrayList<>();
    }

    @Override
    public void notifyObservers(Object o) {
        for (SmokeObserver obserwator : listaObserwatorow) {
            obserwator.update(this, o);
        }
    }

    public void setSmokeDetected(boolean smokeDetected) {
        this.smokeDetected = smokeDetected;
        notifyObservers(smokeDetected);
    }

    public void addSmokeObserver (SmokeObserver o) {
        listaObserwatorow.add(o);
    }

    @Override
    public void acceptVisitor(Visitor v){
        v.performOperation(this);
    }
}
