package org.wzorce;

import java.text.DecimalFormat;

/**
 * Created by natan on 14.04.16.
 */
public class MeanTemperatureVisitor implements Visitor {
    double tempearatureSum = 0;
    int elementsVisited = 0;

    @Override
    public void performOperation(Termometr t) {
        tempearatureSum += t.getCurrentTemperature();
        elementsVisited++;
    }

    @Override
    public void performOperation(SmokeDetector s) {

    }

    public double getMean(){
        return tempearatureSum / elementsVisited;
    }

    public void printMean(){
        System.out.println("Średnia tempearatura termometrów w domu: "
                + new DecimalFormat("#.##").format(getMean())
                + " st C.");
    }
}
