package org.wzorce;

/**
 * Created by natan on 14.04.16.
 */
public class AlarmSetterVisitor implements Visitor{
    FireAlarm fireAlarm;
    public AlarmSetterVisitor(FireAlarm fireAlarm) {
        this.fireAlarm = fireAlarm;
    }

    @Override
    public void performOperation(Termometr t) {
        t.addTemperatureObserver((TemperatureObserver) fireAlarm);
    }

    @Override
    public void performOperation(SmokeDetector s) {
        s.addSmokeObserver((SmokeObserver) fireAlarm);
    }
}
