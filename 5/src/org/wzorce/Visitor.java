package org.wzorce;

/**
 * Created by natan on 14.04.16.
 */
public interface Visitor {
    void performOperation(Termometr t);
    void performOperation(SmokeDetector s);
}
