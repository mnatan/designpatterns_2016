package org.wzorce;

import org.wzorce.Exceptions.RoomNotFoundException;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Observable;
import java.util.Random;
import java.util.stream.IntStream;

/**
 * Created by natan on 14.04.16.
 */
public class Main {

    public static void main(String[] args) throws RoomNotFoundException {
        DetectorNet czujniki = new DetectorNet();

        // init detector net
        Arrays.asList(
                "kuchnia",
                "łazienka",
                "salon"
        ).stream().forEach(
                room -> IntStream.range(0, 2).forEach(
                        i -> {
                            try {
                                czujniki.createRoom(room);
                                czujniki.addDetector(room, new Termometr(randomInRange(18, 25)));
                                czujniki.addDetector(room, new SmokeDetector());
                            } catch (RoomNotFoundException e) {
                                e.printStackTrace();
                            }
                        }));

        AlarmSetterVisitor alarmSetterVisitor = new AlarmSetterVisitor(new FireAlarm(60));
        MeanTemperatureVisitor meanTemperatureVisitor = new MeanTemperatureVisitor();

        Termometr pozar = new Termometr(20);
        SmokeDetector pozardym = new SmokeDetector();
        czujniki.addDetector("kuchnia", pozar);
        czujniki.addDetector("kuchnia", pozardym);

        Iterator i = czujniki.iterator();
        while (i.hasNext()) {
            VisitableElement o = (VisitableElement) i.next();
            o.acceptVisitor(alarmSetterVisitor);
            o.acceptVisitor(meanTemperatureVisitor);
        }

        meanTemperatureVisitor.printMean();

        pozar.setCurrentTemperature(80);
        pozardym.setSmokeDetected(true);

        meanTemperatureVisitor = new MeanTemperatureVisitor();
        i = czujniki.iterator();
        while (i.hasNext()) {
            VisitableElement o = (VisitableElement) i.next();
            o.acceptVisitor(meanTemperatureVisitor);
        }

        meanTemperatureVisitor.printMean();

    }

    public static double randomInRange(double min, double max) {
        Random r = new Random();
        return min + (max - min) * r.nextDouble();
    }
}
