package org.wzorce;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by natan on 14.04.16.
 */
public class FireAlarm implements TemperatureObserver, SmokeObserver {
    private int maxTemperature;

    public FireAlarm(int maxTemp) {
        maxTemperature = maxTemp;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof Termometr) {
            double temp = (double) arg;
            if (maxTemperature < temp) {
                System.out.println("[ALARM] Wysoka temperatura! (" + temp + ")");
            }
        } else if (o instanceof SmokeDetector){
            boolean smoke = (boolean) arg;
            if (smoke) {
                System.out.println("[ALARM] Wykryto dym!");
            }
        }
    }

}
