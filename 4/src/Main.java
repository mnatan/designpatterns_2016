import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import java.awt.*;
import java.awt.event.*;

public class Main extends JFrame implements Receiver, ActionListener {
    private JTextArea ta;
    private JMenuBar menuBar;
    private JMenu fileM, editM, viewM;
    private JScrollPane scpane;
    private JMenuItem exitI, cutI, copyI, pasteI, selectI, saveI, loadI, statusI;
    private String pad;
    private JToolBar toolBar;
    private Invoker editor;

    public Main() {
        super("Turbo text editor");
        setSize(600, 600);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        Container pane = getContentPane();
        pane.setLayout(new BorderLayout());

        editor = new Invoker();

        pad = "asdfasdfadsf\nasdfasdfadsf\nasdfasdfadsf\nasdfasdfadsf\nasdfasdfadsf\nasdfasdfadsf\n";
        ta = new JTextArea(pad); //textarea
        menuBar = new JMenuBar(); //menubar
        fileM = new JMenu("File"); //file menu
        editM = new JMenu("Edit"); //edit menu
        viewM = new JMenu("View"); //edit menu
        scpane = new JScrollPane(ta); //scrollpane  and add textarea to scrollpane
        exitI = new JMenuItem("Exit");
        cutI = new JMenuItem("Cut");
        copyI = new JMenuItem("Copy");
        pasteI = new JMenuItem("Paste");
        selectI = new JMenuItem("Select All"); //menuitems
        saveI = new JMenuItem("Save"); //menuitems
        loadI = new JMenuItem("Load"); //menuitems
        statusI = new JMenuItem("Status"); //menuitems
        toolBar = new JToolBar();

        ta.setLineWrap(true);
        ta.setWrapStyleWord(true);

        setJMenuBar(menuBar);
        menuBar.add(fileM);
        menuBar.add(editM);
        menuBar.add(viewM);

        fileM.add(saveI);
        fileM.add(loadI);
        fileM.add(exitI);

        editM.add(cutI);
        editM.add(copyI);
        editM.add(pasteI);
        editM.add(selectI);

        viewM.add(statusI);

        saveI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
        loadI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, InputEvent.CTRL_MASK));
        cutI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, InputEvent.CTRL_MASK));
        copyI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK));
        pasteI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, InputEvent.CTRL_MASK));
        selectI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK));

        ta.addKeyListener(new KeyListener() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_F2) {
                    try {

                        editor.executeCommand(new CommentCommand(ta));

                    } catch (BadLocationException e1) {
                        e1.printStackTrace();
                    }
                    e.consume();
                } else if ((e.getKeyCode() == KeyEvent.VK_U)
                        && ((e.getModifiers() & KeyEvent.CTRL_MASK) != 0)) {
                    editor.undoCommand();
                } else if ((e.getKeyCode() == KeyEvent.VK_D)
                        && ((e.getModifiers() & KeyEvent.CTRL_MASK) != 0)) {
                    try {
                        editor.executeCommand(new LineDeleteCommand(ta));
                    } catch (BadLocationException e1) {
                        e1.printStackTrace();
                    }
                }
            }

            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }
        });

        pane.add(scpane, BorderLayout.CENTER);
        pane.add(toolBar, BorderLayout.SOUTH);

        saveI.addActionListener(this);
        loadI.addActionListener(this);
        exitI.addActionListener(this);
        cutI.addActionListener(this);
        copyI.addActionListener(this);
        pasteI.addActionListener(this);
        selectI.addActionListener(this);
        statusI.addActionListener(this);

        setVisible(true);
    }

    public static void main(String[] args) {
        new Main();
    }

    public void actionPerformed(ActionEvent e) {
        JMenuItem choice = (JMenuItem) e.getSource();

        if (choice == saveI) {

            throw new NotImplementedException();

        } else if (choice == exitI)

            System.exit(0);

        else if (choice == cutI) {

            pad = ta.getSelectedText();
            ta.replaceRange("", ta.getSelectionStart(), ta.getSelectionEnd());

        } else if (choice == copyI)

            pad = ta.getSelectedText();

        else if (choice == pasteI)

            ta.insert(pad, ta.getCaretPosition());

        else if (choice == selectI)

            ta.selectAll();

        else if (e.getSource() == statusI) {

            throw new NotImplementedException();

        }
    }
}