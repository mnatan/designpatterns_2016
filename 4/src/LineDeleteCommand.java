import javax.swing.*;
import javax.swing.text.BadLocationException;

/**
 * Created by natan on 06.04.16.
 */
public class LineDeleteCommand implements Command {
    private JTextArea ta;
    private int lineNumber = 0;
    private int caretReturn = 0;
    private int start;
    private int end;
    private String removed = "";

    public LineDeleteCommand(JTextArea ta) throws BadLocationException {
        this.ta = ta;
        this.caretReturn = ta.getSelectionStart();
        this.lineNumber = ta.getLineOfOffset(caretReturn);
        System.out.println("line number = " + lineNumber);
        this.start = ta.getLineStartOffset(lineNumber);
        System.out.println("start = " + start);
        this.end = ta.getLineEndOffset(lineNumber);
        System.out.println("end = " + end);
    }

    @Override
    public void execute() {
        try {
            removed = ta.getText(start, end - start);
        } catch (BadLocationException e) {
            e.printStackTrace();
        }
        ta.replaceRange("", start, end);
    }

    @Override
    public void undo() {
        ta.insert(removed, start);
        ta.setCaretPosition(caretReturn);
    }
}
