import java.util.Stack;

public class Invoker {
    private Stack<Command> cs;

    public Invoker() {
        this.cs = new Stack<>();
    }

    public void executeCommand(Command c) {
        c.execute();
        cs.push(c);
    }
    public void undoCommand(){
        if (! cs.empty()) {
            Command c = cs.pop();
            c.undo();
        }
    }
}
