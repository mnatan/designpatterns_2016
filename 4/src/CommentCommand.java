import javax.swing.*;
import javax.swing.text.BadLocationException;

public class CommentCommand implements Command {
    private int lineNumber = 0;
    private int caretReturn = 0;
    private JTextArea ta;

    public CommentCommand(JTextArea ta) throws BadLocationException {
        this.caretReturn = ta.getSelectionStart();
        this.lineNumber = ta.getLineOfOffset(caretReturn);
        this.ta = ta;
    }

    @Override
    public void execute() {
        int offset = 0;
        try {
            offset = ta.getLineStartOffset(lineNumber);
        } catch (BadLocationException e) {
            e.printStackTrace();
        }
        ta.insert("// ", offset);
    }

    @Override
    public void undo() {
        int offset = 0;
        try {
            offset = ta.getLineStartOffset(lineNumber);
        } catch (BadLocationException e) {
            e.printStackTrace();
        }
        ta.replaceRange("", offset, offset + 3);
        ta.setCaretPosition(caretReturn);
    }
}
