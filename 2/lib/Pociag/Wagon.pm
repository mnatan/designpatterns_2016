package Pociag::Wagon;
use Moose;
use Pociag::Element;
extends 'Pociag::Element';

sub to_string {
    my $self = shift;
    return "W" x $self->length;
}

69;
