package Pociag::Lokomotywa;
use Moose;
use Pociag::Element;
extends 'Pociag::Element';

has 'color' => ( is => 'ro', default => 'red' );

sub to_string {
    my $self = shift;
    return "P" . "=" x ( $self->length - 2 ) . "P";
}

69;
