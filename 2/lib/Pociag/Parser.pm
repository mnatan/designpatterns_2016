package Pociag::Parser;
use Moose;
use Pociag::Builder;
use feature qw/say/;

sub create_pociag {
    my $self  = shift;
    my $input = shift;
    my $type  = shift;

    my %types = (
        text    => \&create_as_text,
        picture => \&create_as_picture,
    );

    die if not exists $types{$type};
    $types{$type}->($input, $type);
}

sub create_as_text {
    my ($input, $type) = @_;
    my $builder = Pociag::Builder->new(representation => $type);
    for my $el ( split ',', $input ) {
        $builder->build_part($el);
    }
    return $builder->get_result;
}

sub create_as_picture {
    my ($input, $type) = @_;
    my $builder = Pociag::Builder->new(representation => $type);
    for my $el ( split ',', $input ) {
        $builder->build_part($el);
    }
    return $builder->get_result;
}

69;
