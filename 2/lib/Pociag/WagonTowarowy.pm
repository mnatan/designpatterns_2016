package Pociag::WagonTowarowy;
use Moose;
use Pociag::Wagon;
extends 'Pociag::Wagon';

has 'color' => ( is => 'ro', default => 'green' );

sub to_string {
    my $self = shift;
    return "T" x $self->length;
}

69;
