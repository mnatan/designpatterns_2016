package Pociag::WagonOsobowy;
use Moose;
use Pociag::Wagon;
extends 'Pociag::Wagon';

has 'color' => ( is => 'ro', default => 'blue' );

sub to_string {
    my $self = shift;
    return "O" x $self->length;
}

69;
