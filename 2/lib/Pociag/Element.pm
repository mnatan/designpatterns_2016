package Pociag::Element;
use Moose;

has 'length' => (
    is       => 'rw',
    required => 1,
);

sub to_string {
	my $self = shift;
    return "X" x $self->length;
}

69;
