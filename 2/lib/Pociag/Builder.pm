package Pociag::Builder;
use Moose;
use Pociag;
use Pociag::Lokomotywa;
use Pociag::WagonTowarowy;
use Pociag::WagonOsobowy;

has 'being_built' => (
    is     => 'rw',
    isa    => 'Pociag',
    reader => 'get_result',
    writer => 'set_result',
);

has 'representation' => (
    is       => 'rw',
    required => 1,
);

sub BUILD {
    my $self = shift;
    $self->set_result(
        Pociag->new( representation => $self->representation ) );
}

sub build_part {
    my $self  = shift;
    my $wagon = shift;

    if ( $wagon =~ /l(\d+)/ ) {
        $self->get_result->add_wagon(
            Pociag::Lokomotywa->new( length => $1 ) );
    }
    elsif ( $wagon =~ /wt(\d+)/ ) {
        $self->get_result->add_wagon(
            Pociag::WagonTowarowy->new( length => $1 ) );
    }
    elsif ( $wagon =~ /wo(\d+)/ ) {
        $self->get_result->add_wagon(
            Pociag::WagonOsobowy->new( length => $1 ) );
    }
}

69;
