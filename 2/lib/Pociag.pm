package Pociag;
use Moose;
use GD::Simple;
use feature qw/say/;

has 'wagony' => (
    is      => 'rw',
    isa     => 'ArrayRef[ElementPociagu]',
    default => sub { [] },
);

has 'representation' => (
    is       => 'rw',
    required => 1,
);

sub empty {
    my ($self) = @_;
    if   ( @{ $self->wagony } == 0 ) { return 1 }
    else                             { return 0 }
}

sub add_wagon {
    my ( $self, $wagon ) = @_;

    if ( $self->empty and not $wagon->isa('Pociag::Lokomotywa') ) {
        die "Pierwszym elementem wagonu musi być lokomotywa!\n";
    }

    if ( not $self->empty and $wagon->isa('Pociag::Lokomotywa') ) {
        die "Lokomotywa w środku pociągu nie ma sensu!\n";
    }

    push @{ $self->wagony }, $wagon;
}

sub to_string {
    my $self = shift;
    my @out;
    for my $wagon ( @{ $self->wagony } ) {
        push @out, $wagon->to_string;
    }
    return join "_", @out;
}

sub get_length {
    my $self = shift;
    my $length;
    for my $wagon ( @{ $self->wagony } ) {
        $length += $wagon->length + 1;
    }
    return $length + 1;
}

sub draw {
    my $self    = shift;
    my $length  = $self->get_length;
    my $current = 10;

    my $img = GD::Simple->new( $length * 10, 70 );

    for my $wagon ( @{ $self->wagony } ) {

        $img->bgcolor( $wagon->color );
        $img->fgcolor('black');
        $img->rectangle( $current, 10, $current + $wagon->length * 10, 60 );
        $current += $wagon->length * 10 + 10;

    }
    open my $out, '>', 'img.png' or die;
    binmode $out;
    print $out $img->png;
    close $out;
    system("xdg-open img.png");
}

sub display {
    my $self = shift;
    if ( $self->representation eq "text" ) { print $self->to_string, "\n" }
    elsif ( $self->representation eq "picture" ) { $self->draw }
}

69;
