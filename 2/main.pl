#!/usr/bin/env perl

use lib "./lib";
use Pociag::Parser;
use feature qw/say/;

my @inputs = (    #inputs
    "l5, wo7, wt9",
    "l5, wo7, wt9, wo7, wt9, wo7, wt9, wo7, wt9",
);

for my $input (@inputs) {
    my $parser = Pociag::Parser->new;
    my $pociag = $parser->create_pociag( $input, "picture" );
    say $input;
    say $pociag->to_string;
	$pociag->draw;
}
