package mnatan;

/**
 * Created by natan on 17.03.16.
 */
public class WlacznikFasada extends Wlacznik {

    private int iloscTurbin;
    private SterownikPompyOleju SterownikPompyOleju;
    private SterownikHamulca SterownikHamulca;
    private SterownikGlownyWlacznikPradowy SterownikGlownyWlacznikPradowy;
    private SterownikWlacznikPraduWzbudzania SterownikWlacznikPraduWzbudzania;

    public WlacznikFasada(int iloscTurbin) {
        this.iloscTurbin = iloscTurbin;
        SterownikPompyOleju = new SterownikPompyOleju();
        SterownikHamulca = new SterownikHamulca();
        SterownikGlownyWlacznikPradowy = new SterownikGlownyWlacznikPradowy();
        SterownikWlacznikPraduWzbudzania = new SterownikWlacznikPraduWzbudzania();
    }

    @Override
    public void wlacz (int numerTurbiny){
        if(numerTurbiny<iloscTurbin){
            System.out.println(" >>> Włączanie turbiny "+numerTurbiny+".");
            SterownikPompyOleju.wlacz(numerTurbiny);
            SterownikHamulca.wylacz(numerTurbiny);
            SterownikGlownyWlacznikPradowy.wlacz(numerTurbiny);
            SterownikWlacznikPraduWzbudzania.wlacz(numerTurbiny);
            SterownikWlacznikPraduWzbudzania.wylacz(numerTurbiny);
        } else {
            System.out.println(" >>> Próbowano włączyć nieprawidłową turbinę: " + numerTurbiny + ".");
        }
    }

    @Override
    public void wylacz(int numerTurbiny) {
        if(numerTurbiny<iloscTurbin) {
            System.out.println(" >>> Wyłączanie turbiny " + numerTurbiny + ".");
            SterownikGlownyWlacznikPradowy.wylacz(numerTurbiny);
            SterownikHamulca.wlacz(numerTurbiny);
            SterownikPompyOleju.wylacz(numerTurbiny);
        } else {
            System.out.println(" >>> Próbowano włączyć nieprawidłową turbinę: " + numerTurbiny + ".");
        }
    }

}
