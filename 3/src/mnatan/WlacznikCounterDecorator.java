package mnatan;

/**
 * Created by natan on 17.03.16.
 */
public class WlacznikCounterDecorator extends Wlacznik{

    private Wlacznik primary;
    private int timesUsed = 0;

    public WlacznikCounterDecorator(Wlacznik primary) {
        this.primary = primary;
    }

    private void reportUsage(){
        timesUsed++;
        System.out.println(" >>> Wlacznik "+primary.getClass()+" został przełączony po raz "+timesUsed);
    }

    @Override
    public void wlacz(int numerTurbiny) {
        reportUsage();
        primary.wlacz(numerTurbiny);
    }

    @Override
    public void wylacz(int numerTurbiny) {
        reportUsage();
        primary.wylacz(numerTurbiny);
    }

}
