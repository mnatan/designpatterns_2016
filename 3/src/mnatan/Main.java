package mnatan;

public class Main {

    public static void main(String[] args) {
        Wlacznik glowny_wlacznik = new WlacznikFasada(2);
        glowny_wlacznik = new WlacznikCounterDecorator(glowny_wlacznik);
        glowny_wlacznik.wlacz(1);
        glowny_wlacznik.wylacz(1);
        glowny_wlacznik.wlacz(0);
        glowny_wlacznik.wylacz(0);
        glowny_wlacznik.wylacz(6);
    }

}
