package mnatan;

/**
 * Created by natan on 17.03.16.
 */
public abstract class Wlacznik {

    public void wlacz (int numerTurbiny){
        System.out.println(this.getClass() + " ON w turbinie: "+ numerTurbiny +".");
    }

    public void wylacz (int numerTurbiny){
        System.out.println(this.getClass() + " OFF w turbinie: "+ numerTurbiny +".");
    }

}
