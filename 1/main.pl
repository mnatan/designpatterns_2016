#!/usr/bin/env perl
package main;

use Data::Dumper;
use feature qw/say/;

use FindBin;
use Cwd qw/realpath/;
use lib realpath("$FindBin::Bin/lib");

use FileHandler;
use Format::SpaceSeparated;
use Format::XMLLike;
use Sort::SystemSort;
use Sort::StableSort;

sub main {
    my $file_handler = FileHandler->new;

    my $space_separated   = Format::SpaceSeparated->new;
    my $sorting_algorithm = Sort::SystemSort->new;

    $file_handler->set_format($space_separated);
    $file_handler->set_algorithm($sorting_algorithm);
    $file_handler->read("test.txt");

    $file_handler->sort();

    my $xml_like = Format::XMLLike->new( tag => "value" );

    $file_handler->set_format($xml_like);
    $file_handler->save("test2.txt");
    $file_handler->read("test2.txt");
    $sorting_algorithm = Sort::StableSort->new;
    $file_handler->set_algorithm($sorting_algorithm);
    $file_handler->sort();
    $file_handler->set_format($space_separated);
    $file_handler->save("test3.txt");
}
main();
