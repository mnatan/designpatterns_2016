package FileHandler;
use Moose;

has 'format' => (
    is     => 'ro',
    isa    => 'Format::FileFormat',
    writer => 'set_format',
);
has 'algorithm' => (
    is     => 'ro',
    isa    => 'Sort::SortingAlgorithm',
    writer => 'set_algorithm',
);
has 'filename' => (
    is     => 'ro',
    writer => 'set_filename',
);
has 'data' => (
    is     => 'ro',
    isa    => 'ArrayRef[Int]',
    writer => 'set_data',
);

sub read {
    my (    #args
        $self,
        $filename,
    ) = @_;
    $filename //= $self->filename;
    $self->set_filename($filename);

    open my $fh, "<", $self->filename
        or die "No such file: " . $self->filename;

    $self->set_data( $self->format->read( $fh, $self->data ) );

    close $fh;

}

sub sort {
    my (    #args
        $self,
    ) = @_;

    # print join ", ", @{ $self->data };
    # print "\n";

    $self->algorithm->sort( $self->data );

    # print join ", ", @{ $self->data };
    # print "\n";
}

sub save {
    my (    #args
        $self,
        $filename,
    ) = @_;
    $filename //= $self->filename;
    $self->set_filename($filename);

    open my $fh, ">", $self->filename
        or die "No such file: " . $self->filename;

    $self->format->write( $fh, $self->data );

    close $fh;
}

#    my $file_handler = FileHandler->new;
#
#    my $space_separated   = Format::SpaceSeparated->new;
#    my $sorting_algorithm = Sort::SystemSort->new;
#
#    $file_handler->set_format($space_separated);
#    $file_handler->set_algorithm($sorting_algorithm);
#    $file_handler->read("test.txt");
#
#    $file_handler->sort($sorting_algorithm);
#
#    my $xml_like = Format::XMLLike->new;
#    $file_handler->set_format($xml_like);
#    $file_handler->save($sorting_algorithm);

1;
