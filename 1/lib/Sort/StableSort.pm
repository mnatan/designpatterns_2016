package Sort::StableSort;
use Moose;

extends 'Sort::SortingAlgorithm';

sub sort {
    my ( $self, $dataref ) = @_;
    @{$dataref} = sort { $b <=> $a } @{$dataref};
}

1;
