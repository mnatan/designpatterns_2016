package Sort::SystemSort;
use Moose;

extends 'Sort::SortingAlgorithm';

sub sort {
    my ( $self, $dataref ) = @_;
    @{$dataref} = sort { $a <=> $b } @{$dataref};
}

1;
