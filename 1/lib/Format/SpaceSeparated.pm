package Format::SpaceSeparated;
use Moose;

extends 'Format::FileFormat';

sub read {
    my ( $self, $filehandle, $dataref ) = @_;
	$dataref //= [];
    my @tmp = @{$dataref};
    while (<$filehandle>) {
        push @tmp, split;
    }
	return \@tmp;
}

sub write {
    my (    #args
        $self,
        $filehandle,
        $dataref,
    ) = @_;
    for ( @{$dataref} ) {
        print $filehandle "$_ ";
    }
}

1;
