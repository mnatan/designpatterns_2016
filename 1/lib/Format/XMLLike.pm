package Format::XMLLike;
use Moose;

extends 'Format::FileFormat';

has 'tag' => (
    is       => 'ro',
    required => 1,
);

sub read {
    my (    #args
        $self,
        $filehandle,
        $dataref,
    ) = @_;
    $dataref //= [];
    my $tag = $self->tag;
    while (<$filehandle>) {
        $_ =~ m{<$tag>\s*(\d+)\s*</$tag>}g;
        push @{$dataref}, $1;
    }
    return $dataref;
}

sub write {
    my (    #args
        $self,
        $filehandle,
        $dataref,
    ) = @_;
    for ( @{$dataref} ) {
        print $filehandle "<" . $self->tag . ">";
        print $filehandle "$_";
        print $filehandle "</" . $self->tag . ">";
        print $filehandle "\n";
    }
}

1;
